package ninja.robbert.gateway;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class GatewayConfig {

	private Map<String, String> routes = new HashMap<>();

	public Map<String, String> getRoutes() {
		return routes;
	}
	public GatewayConfig(){
		this.routes.put("service1","http://localhost:8081");
	}
	public String backend(String service) {
		return routes.get(service);
	}
}
